//
//  WelcomeViewController.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/17.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: WelcomeViewController
// MARK: -
/******************************************************************************/

class WelcomeViewController: UIViewController {
    
    
    //------------------------------------------------------------------------//
    // MARK: Constants
    //------------------------------------------------------------------------//
    
    let kWelcomeText = "Welcome to HelloDoc!"
    let kSubtitleText = "Start setting up your bio that\npatients will see"
    let kGoButtonText = "Let's go!"
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var goButton: UIButton!
    
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    override init() {
        super.init(nibName: "WelcomeViewController", bundle: nil)
        
        title = ""
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Did Load
    //------------------------------------------------------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        
        // setup sub-views
        welcomeLabel.font = FontConstants.helveticaNeueRegular(FontConstants.fontSize25)
        welcomeLabel.textColor = UIColor.darkTextColor()
        welcomeLabel.backgroundColor = UIColor.clearColor()
        welcomeLabel.text = kWelcomeText
        welcomeLabel.textAlignment = NSTextAlignment.Center
        
        subtitleLabel.font = FontConstants.helveticaNeueLight(FontConstants.fontSize17)
        subtitleLabel.textColor = UIColor.darkTextColor()
        subtitleLabel.backgroundColor = UIColor.clearColor()
        subtitleLabel.text = kSubtitleText
        subtitleLabel.textAlignment = NSTextAlignment.Center
        
        goButton.backgroundColor = ColorConstants.helloDocBlue
        goButton.setTitle(kGoButtonText, forState: UIControlState.Normal)
        goButton.titleLabel?.font = FontConstants.helveticaNeueLight(FontConstants.fontSize17)
        goButton.layer.cornerRadius = LayoutConstants.authButtonCornerRadius
        goButton.layer.masksToBounds = true
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Layout Subviews
    //------------------------------------------------------------------------//
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // setting subtitle's max width programatically
        subtitleLabel.preferredMaxLayoutWidth = welcomeLabel.frame.size.width
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Transitions
    //------------------------------------------------------------------------//
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Actions
    //------------------------------------------------------------------------//
    
    @IBAction func goButtonTapped(sender: AnyObject) {
        navigationController?.pushViewController(BasicInfoViewController(), animated: true)
    }
    
}