//
//  BasicInfoViewController.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/17.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: BasicInfoViewController
// MARK: -
/******************************************************************************/

class BasicInfoViewController: UIViewController, UITextFieldDelegate {
    
    
    //------------------------------------------------------------------------//
    // MARK: Constants
    //------------------------------------------------------------------------//
    
    let kTitleText = "Basic Info"
    let kSubtitleText = "1 of 5"
    let kNextText = "Next"

    let kHeaderText = "My name is..."
    let kFirstNamePlaceholder = "First name"
    let kMiddleNamePlaceholder = "Middle name"
    let kLastNamePlaceholder = "Last name"
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var firstNameTextField: BottomBorderedTextField!
    @IBOutlet weak var middleNameTextField: BottomBorderedTextField!
    @IBOutlet weak var lastNameTextField: BottomBorderedTextField!
    
    var nextButton: UIBarButtonItem!
    
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    override init() {
        super.init(nibName: "BasicInfoViewController", bundle: nil)
        
        nextButton = UIBarButtonItem(title: kNextText, style: UIBarButtonItemStyle.Plain, target: self, action: "nextButtonTapped:")
        nextButton.enabled = false
        
        navigationItem.hidesBackButton = true
        navigationItem.titleView = NavigationConstants.titleView(kTitleText, subText: kSubtitleText)
        navigationItem.rightBarButtonItem = nextButton
        edgesForExtendedLayout = UIRectEdge.None
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Did Load
    //------------------------------------------------------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "viewTapped:")
        view.addGestureRecognizer(tapGestureRecognizer)
        view.backgroundColor = UIColor.whiteColor()
        
        // setup sub-views
        headerLabel.font = FontConstants.helveticaNeueRegular(FontConstants.fontSize17)
        headerLabel.textColor = UIColor.darkTextColor()
        headerLabel.backgroundColor = UIColor.clearColor()
        headerLabel.text = kHeaderText
        headerLabel.textAlignment = NSTextAlignment.Left
        
        firstNameTextField.placeholder = kFirstNamePlaceholder
        firstNameTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        firstNameTextField.textColor = ColorConstants.darkTextColor
        firstNameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        firstNameTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        firstNameTextField.delegate = self
        
        middleNameTextField.placeholder = kMiddleNamePlaceholder
        middleNameTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        middleNameTextField.textColor = ColorConstants.darkTextColor
        middleNameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        middleNameTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        middleNameTextField.delegate = self
        
        lastNameTextField.placeholder = kLastNamePlaceholder
        lastNameTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        lastNameTextField.textColor = ColorConstants.darkTextColor
        lastNameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        lastNameTextField.returnKeyType = UIReturnKeyType.Next
        lastNameTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        lastNameTextField.delegate = self
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Transitions
    //------------------------------------------------------------------------//
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//
    
    func viewTapped(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func updateNextButton() {
        if countElements(firstNameTextField.text) > 0 && countElements(middleNameTextField.text) > 0 && countElements(lastNameTextField.text) > 0 {
            nextButton.enabled = true
        } else {
            nextButton.enabled = false
        }
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Actions
    //------------------------------------------------------------------------//
    
    func textFieldDidChange(sender: UITextField) {
        updateNextButton()
    }
    
    func nextButtonTapped(sender: UIBarButtonItem) {
        println("[Debug] next button tapped")
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Text Field Delegate
    //------------------------------------------------------------------------//
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            middleNameTextField.becomeFirstResponder()
        } else if textField == middleNameTextField {
            lastNameTextField.becomeFirstResponder()
        } else if textField == lastNameTextField {
            view.endEditing(true)
        }
        return true
    }
}