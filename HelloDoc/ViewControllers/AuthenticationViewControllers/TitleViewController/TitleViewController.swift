//
//  TitleViewController.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: TitleViewController
// MARK: -
/******************************************************************************/

class TitleViewController: UIViewController {
    
    
    //------------------------------------------------------------------------//
    // MARK: Constants
    //------------------------------------------------------------------------//
    
    private let kTitleText: String = "HelloDoc"
    private let kSignupForDoctorsText: String = "Sign up as Doctors"
    private let kSignupForAdminsText: String = "Sign up as Admins"
    private let kLoginTextHead: String = "Have an account? "
    private let kLoginTextTail: String = "Login here"
    
    private let kTitleIcon: String = "title_icon"
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    @IBOutlet private weak var titleIconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var adminSignUpButton: UIButton!
    @IBOutlet private weak var doctorSignUpButton: UIButton!

    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    override init() {
        super.init(nibName: "TitleViewController", bundle: nil)
        
        title = ""
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Did Load
    //------------------------------------------------------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // setup sub-views
        titleIconImageView.image = UIImage(named: kTitleIcon)
        titleIconImageView.backgroundColor = UIColor.clearColor()

        titleLabel.font = FontConstants.nunitoBold(FontConstants.fontSize37)
        titleLabel.textColor = ColorConstants.darkTextColor
        titleLabel.backgroundColor = UIColor.clearColor()
        
        doctorSignUpButton.backgroundColor = ColorConstants.helloDocBlue
        doctorSignUpButton.setTitle(kSignupForDoctorsText, forState: UIControlState.Normal)
        doctorSignUpButton.titleLabel?.font = FontConstants.helveticaNeueLight(FontConstants.fontSize17)
        doctorSignUpButton.layer.cornerRadius = LayoutConstants.authButtonCornerRadius
        doctorSignUpButton.layer.masksToBounds = true
        
        adminSignUpButton.backgroundColor = ColorConstants.helloDocBlue
        adminSignUpButton.setTitle(kSignupForAdminsText, forState: UIControlState.Normal)
        adminSignUpButton.titleLabel?.font = FontConstants.helveticaNeueLight(FontConstants.fontSize17)
        adminSignUpButton.layer.cornerRadius = LayoutConstants.authButtonCornerRadius
        adminSignUpButton.layer.masksToBounds = true
        
        var loginStringHead: NSAttributedString = NSAttributedString(string: kLoginTextHead, attributes: [
            NSFontAttributeName: FontConstants.helveticaNeueLight(FontConstants.fontSize17),
            NSForegroundColorAttributeName: ColorConstants.darkTextColor
        ])
        
        var loginStringTail: NSAttributedString = NSAttributedString(string: kLoginTextTail, attributes: [
            NSFontAttributeName: FontConstants.helveticaNeueBold(FontConstants.fontSize17),
            NSForegroundColorAttributeName: ColorConstants.helloDocBlue
        ])
        
        var loginString: NSMutableAttributedString = NSMutableAttributedString(attributedString: loginStringHead)
        loginString.appendAttributedString(loginStringTail)
        
        loginButton.setAttributedTitle(loginString, forState: UIControlState.Normal)
        
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Transitions
    //------------------------------------------------------------------------//
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Actions
    //------------------------------------------------------------------------//
    
    @IBAction private func doctorSignUpButtonTapped(sender: AnyObject) {
        navigationController?.pushViewController(DoctorSignUpViewController(), animated: true)
    }
    
    @IBAction private func adminSignUpButtonTapped(sender: AnyObject) {
        println("[Debug] Admin Sign Up Button Tapped")
    }

    @IBAction private func loginButtonTapped(sender: AnyObject) {
        println("[Debug] Login Button Tapped")
    }
}