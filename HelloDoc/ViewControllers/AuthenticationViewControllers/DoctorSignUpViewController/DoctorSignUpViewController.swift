//
//  DoctorSignUpViewController.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: DoctorSignUpViewController
// MARK: -
/******************************************************************************/

class DoctorSignUpViewController: UIViewController, UITextFieldDelegate {
    
    
    //------------------------------------------------------------------------//
    // MARK: Constants
    //------------------------------------------------------------------------//
    
    let kTitleText: String = "Sign up"
    let kNextText: String = "Next"
    let kPRCPlaceholder: String = "PRC Number"
    let kEmailPlaceholder: String = "Email address"
    let kPasswordPlaceholder: String = "Password"
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    @IBOutlet weak var prcNumberTextField: BottomBorderedTextField!
    @IBOutlet weak var emailTextField: BottomBorderedTextField!
    @IBOutlet weak var passwordTextField: BottomBorderedTextField!
    
    var nextButton: UIBarButtonItem!
    
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    override init() {
        super.init(nibName: "DoctorSignUpViewController", bundle: nil)
        
        nextButton = UIBarButtonItem(title: kNextText, style: UIBarButtonItemStyle.Plain, target: self, action: "nextButtonTapped:")
        nextButton.enabled = false
        
        navigationItem.titleView = NavigationConstants.titleView(kTitleText)
        navigationItem.rightBarButtonItem = nextButton
        edgesForExtendedLayout = UIRectEdge.None
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Did Load
    //------------------------------------------------------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "viewTapped:")
        view.addGestureRecognizer(tapGestureRecognizer)
        view.backgroundColor = UIColor.whiteColor()
        
        // setup sub-views
        prcNumberTextField.placeholder = kPRCPlaceholder
        prcNumberTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        prcNumberTextField.textColor = ColorConstants.darkTextColor
        prcNumberTextField.keyboardType = UIKeyboardType.NumberPad
        prcNumberTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        prcNumberTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        prcNumberTextField.delegate = self
        
        emailTextField.placeholder = kEmailPlaceholder
        emailTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        emailTextField.textColor = ColorConstants.darkTextColor
        emailTextField.keyboardType = UIKeyboardType.EmailAddress
        emailTextField.autocorrectionType = UITextAutocorrectionType.No
        emailTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        emailTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        emailTextField.delegate = self
        
        passwordTextField.placeholder = kPasswordPlaceholder
        passwordTextField.font = FontConstants.helveticaNeueLight(FontConstants.fontSize15)
        passwordTextField.textColor = ColorConstants.darkTextColor
        passwordTextField.secureTextEntry = true
        passwordTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTextField.returnKeyType = UIReturnKeyType.Join
        passwordTextField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        passwordTextField.delegate = self
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Transitions
    //------------------------------------------------------------------------//
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//
    
    func updateNextButton() {
        if countElements(prcNumberTextField.text) > 0 && countElements(emailTextField.text) > 0 && countElements(passwordTextField.text) > 0 {
            nextButton.enabled = true
        } else {
            nextButton.enabled = false
        }
    }
    
    func signUp() {
        // TODO: do signup here
        
        ContainerViewController.container.showOnboardingScreen(true, completion: nil)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Actions
    //------------------------------------------------------------------------//
    
    func viewTapped(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func nextButtonTapped(sender: UIBarButtonItem) {
        signUp()
    }
    
    func textFieldDidChange(sender: UITextField) {
        updateNextButton()
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Text Field Delegate
    //------------------------------------------------------------------------//
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == prcNumberTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            view.endEditing(true)
            if nextButton.enabled {
                signUp()
            }
        }
        return true
    }
}