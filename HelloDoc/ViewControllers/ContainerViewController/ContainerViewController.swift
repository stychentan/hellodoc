//
//  ContainerViewController.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: ContainerViewController
// MARK: -
/******************************************************************************/

class ContainerViewController: UIViewController {
    
    var authenticationNavigationController: UINavigationController!
    var onboardingNavigationController: UINavigationController!
    
    
    //------------------------------------------------------------------------//
    // MARK: Singleton Initialization
    //------------------------------------------------------------------------//
    
    class var container: ContainerViewController {
        struct Singleton {
            static let container = ContainerViewController()
        }
        return Singleton.container;
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: View Did Load
    //------------------------------------------------------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.blackColor()
        
        authenticationNavigationController = UINavigationController()
        authenticationNavigationController.view.backgroundColor = UIColor.clearColor()
        authenticationNavigationController.navigationBar.translucent = false
        
        onboardingNavigationController = UINavigationController()
        onboardingNavigationController.view.backgroundColor = UIColor.clearColor()
        onboardingNavigationController.navigationBar.translucent = false
        
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//
    
    
    /** 
        Show Title Screen.

        :param: animated Show with animation or not
        :param: completion (Optional) Completion closure after showing
    */
    func showTitleScreen(animated: Bool, completion: (()->Void)?) {
        
        weak var __self = self
        onboardingNavigationController.viewControllers = [WelcomeViewController()]
        presentViewController(onboardingNavigationController, animated: false) { () -> Void in
            self.authenticationNavigationController.viewControllers = [TitleViewController()]
            self.onboardingNavigationController.presentViewController(self.authenticationNavigationController, animated: animated, completion: completion)
        }
    }
    
    func hideTitleScreen(animated: Bool, completion: (()->Void)?) {
        dismissViewControllerAnimated(animated, completion: completion)
    }
    
    func showOnboardingScreen(animated: Bool, completion: (()->Void)?) {
        onboardingNavigationController.dismissViewControllerAnimated(animated, completion: completion)
    }
    
    func hideOnboardingScreen(animated: Bool, completion: (()->Void)?) {
        hideTitleScreen(animated, completion: completion)
    }
}