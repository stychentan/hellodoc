//
//  HelloDoc-Bridging-Header.h
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/16.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

#ifndef HelloDoc_HelloDoc_Bridging_Header_h
#define HelloDoc_HelloDoc_Bridging_Header_h

#include <Foundation/Foundation.h> // needed to add NSBlock support

#import "AFNetworking.h"
#import "CoreData+MagicalRecord.h"
#import "MAKVONotificationCenter.h"
#import "SSKeychain.h"

#endif
