//
//  Constants.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: Constants
// MARK: -
/******************************************************************************/


//------------------------------------------------------------------------//
// MARK: FontConstants
//------------------------------------------------------------------------//


// A collection of constants involving layout
struct LayoutConstants {
    
    static let authButtonCornerRadius: CGFloat = 5.0
    static let borderWeight: CGFloat = 0.5
    
}


// A collection of constants involving layout
struct NotificationConstants {
    
    static let sessionOpened = "sessionOpened"
    static let sessionClosed = "sessionClosed"
    
}


/// A collection of constants involving UIFont
struct FontConstants {
    
    private static let nunitoRegularFontName    = "Nunito-Regular"
    private static let nunitoLightFontName      = "Nunito-Light"
    private static let nunitoBoldFontName       = "Nunito-Bold"
    private static let helveticaNeueRegularFontName = "HelveticaNeue"
    private static let helveticaNeueLightFontName = "HelveticaNeue-Light"
    private static let helveticaNeueBoldFontName = "HelveticaNeue-Bold"
    
    static let fontSize12: CGFloat = 12.0
    static let fontSize15: CGFloat = 15.0
    static let fontSize17: CGFloat = 17.0
    static let fontSize20: CGFloat = 20.0
    static let fontSize25: CGFloat = 25.0
    static let fontSize37: CGFloat = 37.0
    
    /**
        Create a Nunito Regular Font Object
        
        :param: size Font size
    
        :returns: A Nunito Regular Font Object with specified Size
    */
    static func nunitoRegular(size: CGFloat) -> UIFont {
        return UIFont(name: nunitoRegularFontName, size: size)
    }
    
    
    /**
        Create a Nunito Light Font Object
    
        :param: size Font size
    
        :returns: A Nunito Light Font Object with specified Size
    */
    static func nunitoLight(size: CGFloat) -> UIFont {
        return UIFont(name: nunitoLightFontName, size: size)
    }
    
    
    /**
        Create a Nunito Bold Font Object
    
        :param: size Font size
    
        :returns: A Nunito Bold Font Object with specified Size
    */
    static func nunitoBold(size: CGFloat) -> UIFont {
        return UIFont(name: nunitoBoldFontName, size: size)
    }
    
    /**
    Create a Hel Regular Font Object
    
    :param: size Font size
    
    :returns: A Nunito Regular Font Object with specified Size
    */
    static func helveticaNeueRegular(size: CGFloat) -> UIFont {
        return UIFont(name: helveticaNeueRegularFontName, size: size)
    }
    
    
    /**
    Create a Nunito Light Font Object
    
    :param: size Font size
    
    :returns: A Nunito Light Font Object with specified Size
    */
    static func helveticaNeueLight(size: CGFloat) -> UIFont {
        return UIFont(name: helveticaNeueLightFontName, size: size)
    }
    
    
    /**
    Create a Nunito Bold Font Object
    
    :param: size Font size
    
    :returns: A Nunito Bold Font Object with specified Size
    */
    static func helveticaNeueBold(size: CGFloat) -> UIFont {
        return UIFont(name: helveticaNeueBoldFontName, size: size)
    }
}


/// A collection of constants involving UIColor
struct ColorConstants {
    
    static let darkTextColor = UIColor.colorWithHex(hex: "25282b")
    static let lightTextColor = UIColor.colorWithHex(hex: "d0d0d0")
    static let helloDocBlue = UIColor.colorWithHex(hex: "2987df")
    static let borderGray = UIColor.colorWithHex(hex: "a8a9aa")
}


/// A collection of constants involving NavigationBar
struct NavigationConstants {
    
    static func titleView(text: String) -> UILabel {
        var titleLabel: UILabel = UILabel()
        titleLabel.backgroundColor = UIColor.clearColor()
        titleLabel.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.textColor = ColorConstants.darkTextColor
        titleLabel.text = text
        titleLabel.font = FontConstants.helveticaNeueLight(FontConstants.fontSize20)
        
        var size:CGSize = (text as NSString).sizeWithAttributes([NSFontAttributeName: titleLabel.font])
        titleLabel.frame = CGRectMake(0, 0, size.width, size.height)
        return titleLabel
    }
    
    static func titleView(text: String, subText: String) -> UIView {
        let subtitleMargin: CGFloat = 0.0
        
        var view: UIView = UIView()
        view.backgroundColor = UIColor.clearColor()
        view.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
        
        var subtitleLabel: UILabel = UILabel()
        subtitleLabel.backgroundColor = UIColor.clearColor()
        subtitleLabel.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
        subtitleLabel.textAlignment = NSTextAlignment.Center
        subtitleLabel.textColor = ColorConstants.lightTextColor
        subtitleLabel.text = subText
        subtitleLabel.font = FontConstants.helveticaNeueBold(FontConstants.fontSize12)
        subtitleLabel.sizeToFit()
        
        var titleLabel: UILabel = UILabel()
        titleLabel.backgroundColor = UIColor.clearColor()
        titleLabel.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.textColor = ColorConstants.darkTextColor
        titleLabel.text = text
        titleLabel.font = FontConstants.helveticaNeueLight(FontConstants.fontSize20)
        titleLabel.sizeToFit()
        
        // combine view in UIView
        var size: CGSize = CGSizeZero
        size.height = 2 * (subtitleLabel.frame.size.height + subtitleMargin) + titleLabel.frame.size.height
        size.width = max(titleLabel.frame.width, subtitleLabel.frame.width)
        view.frame = CGRectMake(0, 0, size.width, size.height)
        
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        
        var rect: CGRect = titleLabel.frame
        rect.origin.x = (view.frame.size.width - titleLabel.frame.size.width) / 2.0
        rect.origin.y = subtitleLabel.frame.size.height + subtitleMargin
        titleLabel.frame = rect
        
        rect = subtitleLabel.frame
        rect.origin.x = (view.frame.size.width - subtitleLabel.frame.size.width) / 2.0
        rect.origin.y = view.frame.size.height - subtitleLabel.frame.size.height
        subtitleLabel.frame = rect
        
        return view
    }
}


/// A collection of constants involving String
struct StringConstants {
    
    static func isStringValid(string: String) -> Bool {
        return countElements(string) > 0
    }
    
}

