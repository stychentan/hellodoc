//
//  Color+RGB.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: Constants
// MARK: -
/******************************************************************************/

extension UIColor {
    
    
    //------------------------------------------------------------------------//
    // MARK: Color Extensions
    //------------------------------------------------------------------------//
    
    /**
        Create a UIColor object with hex string

        :param: Color Hex String

        :returns: A Color Object with specified hex string color
    */
    class func colorWithHex(#hex: String) -> UIColor {
        var cleanString:NSString = hex.stringByReplacingOccurrencesOfString("#", withString: "") as NSString
        if cleanString.length == 3 {
            cleanString = NSString(format: "%@%@%@%@%@%@",
                cleanString.substringWithRange(NSMakeRange(0,1)), cleanString.substringWithRange(NSMakeRange(0,1)),
                cleanString.substringWithRange(NSMakeRange(1,1)), cleanString.substringWithRange(NSMakeRange(1,1)),
                cleanString.substringWithRange(NSMakeRange(2,1)), cleanString.substringWithRange(NSMakeRange(2,1)))
        }
        if cleanString.length == 6 {
            cleanString = cleanString.stringByAppendingString("ff")
        }
        
        var baseValue: UInt32 = 0
        NSScanner(string: cleanString).scanHexInt(&baseValue)
        
        var red: CGFloat = CGFloat(((baseValue >> 24) & 0xFF)) / CGFloat(255.0)
        var green: CGFloat = CGFloat(((baseValue >> 16) & 0xFF)) / CGFloat(255.0)
        var blue: CGFloat = CGFloat(((baseValue >> 8) & 0xFF)) / CGFloat(255.0)
        var alpha: CGFloat = CGFloat(((baseValue >> 0) & 0xFF)) / CGFloat(255.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}