//
//  AppDelegate.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/14.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: AppDelegate
// MARK: -
/******************************************************************************/

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    //------------------------------------------------------------------------//
    // MARK: Application Delegate
    //------------------------------------------------------------------------//

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
//        printAvailableFonts()
        
        // setup managers
        APIManager.manager
        
        // setup appearances
        setupNavigationBarApperance()
        
        // setup container view controller
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        if let window = window {
            window.backgroundColor = UIColor.whiteColor()
            window.makeKeyAndVisible()
            window.rootViewController = ContainerViewController.container
        }
        
        ContainerViewController.container.showTitleScreen(false, completion: nil)
        
        return true
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//

    private func printAvailableFonts() {
        for family in UIFont.familyNames() {
            println("\(family)")
            for name in UIFont.fontNamesForFamilyName(String(family as NSString)) {
                println("\(name)")
            }
        }
    }
    
    private func setupNavigationBarApperance() {
        
        let backButtonImage = "back_button"
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: backButtonImage)
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: backButtonImage)
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSFontAttributeName: FontConstants.helveticaNeueBold(FontConstants.fontSize17),
            NSForegroundColorAttributeName: ColorConstants.helloDocBlue
        ], forState: UIControlState.Normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSFontAttributeName: FontConstants.helveticaNeueBold(FontConstants.fontSize17),
            NSForegroundColorAttributeName: ColorConstants.helloDocBlue.colorWithAlphaComponent(0.5)
        ], forState: UIControlState.Disabled)
        
    }
}

