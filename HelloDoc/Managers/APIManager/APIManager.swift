//
//  APIManager.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/15.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import Foundation


/******************************************************************************/
// MARK: -
// MARK: APIManager
// MARK: -
/******************************************************************************/


// Constants regarding HelloDoc's API
struct APIConstants {
    static let baseURL = "https://api.hellodoc.ph/"
    static let requestTimeout = 300.0 // in seconds
    static let maxDownloadRequests = 3
    static let maxUploadRequests = 2
}


/**
    API Rest Method Types
    
    - GET
    - HEAD
    - POST
    - PUT
    - PATCH
    - DELETE
*/
public enum APIMethod: String {
    case GET = "GET"
    case HEAD = "HEAD"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
}


/// HelloDoc's API Manager
class APIManager: AFHTTPRequestOperationManager {
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    var creds: Credential!
    var downloadQueue: NSOperationQueue = NSOperationQueue()
    var uploadQueue: NSOperationQueue = NSOperationQueue()
    var httpRequestSerializer: AFHTTPRequestSerializer = AFHTTPRequestSerializer()
    
    private var initialized: Bool = false
    
    
    //------------------------------------------------------------------------//
    // MARK: Singleton Initialization
    //------------------------------------------------------------------------//
    
    class var manager: APIManager {
        struct Singleton {
            static let manager = APIManager(baseURL: NSURL(string: APIConstants.baseURL))
        }
            
        return Singleton.manager;
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    func initialize() {
        if initialized == true { return }
        initialized = true
        
        requestSerializer = AFJSONRequestSerializer()
        responseSerializer = AFJSONResponseSerializer()
        requestSerializer.timeoutInterval = APIConstants.requestTimeout
        
        downloadQueue.maxConcurrentOperationCount = APIConstants.maxDownloadRequests
        uploadQueue.maxConcurrentOperationCount = APIConstants.maxUploadRequests
    }
    
    override init() {
        super.init()
        initialize()
    }
    
    override init(baseURL url: NSURL!) {
        super.init(baseURL: url)
        initialize()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//
    
    
    func setupCredential() {
        creds = Credential(service: APIConstants.baseURL)
        if creds.isValid() {
            // add OAuth Header here
            requestSerializer.setValue("Bearer \(creds.accessToken)", forHTTPHeaderField: "Authorization")
        }
        creds.postNotification()
    }
    
    /**
        Send an HTTP Request
        
        :param: URLString The end point URL relative to the baseURL
        :param: method The REST Method Type
        :param: parameters The parameters for the request
        :param: retry Bool value if request should retry on failure
    */
    func send(
        URLString: String,
        method: APIMethod,
        parameters: AnyObject,
        retry: Bool,
        success: ((operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void)!,
        failure: ((operation: AFHTTPRequestOperation!, error: NSError!) -> Void)!)
    {
        var op: AFHTTPRequestOperation!
        var string: String = NSURL(string: URLString, relativeToURL: baseURL).absoluteString!
        var request: NSURLRequest = requestSerializer.requestWithMethod(string, URLString: URLString, parameters: parameters, error: nil)
        op = HTTPRequestOperationWithRequest(request, success: success, failure: { (operation1, error) -> Void in
            if (retry && self.shouldReloginWithOperation(operation1, error: error)) {
                
            }
        })
    }
    
    func shouldReloginWithOperation(operation: AFHTTPRequestOperation, error: NSError) -> Bool {
        var shouldRetry: Bool = false
        
        if operation.responseObject.isKindOfClass(NSDictionary) {
            var detail: String? = operation.responseObject.objectForKey("detail")?.lowercaseString?
            if detail == "invalid token" {
                shouldRetry = true
            }
        }
        return shouldRetry;
    }
}