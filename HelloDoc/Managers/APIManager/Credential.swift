//
//  Credential.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/16.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import Foundation


/******************************************************************************/
// MARK: -
// MARK: Credential
// MARK: -
/******************************************************************************/


// Constants regarding HelloDoc's Credentials
struct CredConstants {
    static let OAuthCredential = "oauth_credential"
    static let usernameKey = "username"
    static let passwordKey = "password"
    static let accessTokenKey = "access_token"
    static let expiresInKey = "expires_in"
    static let expiryDateKey = "expiry_date"
    static let scopeKey = "scope"
    static let tokenTypeKey = "token_type"
}


/// HelloDoc's credential object
class Credential: NSObject {
    
    
    //------------------------------------------------------------------------//
    // MARK: Properties
    //------------------------------------------------------------------------//
    
    // DO NOT change value outside of Credential.swift
    var username: String!
    var password: String!
    var accessToken: String!
    
    private var credentials: NSDictionary!
    private var service: String!
    
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    init(service: String) {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let creds: NSDictionary? = defaults.objectForKey(CredConstants.OAuthCredential) as? NSDictionary
        let username: String = SSKeychain.passwordForService(service, account: CredConstants.usernameKey)
        let password: String = SSKeychain.passwordForService(service, account: CredConstants.passwordKey)
        let accessToken: String = SSKeychain.passwordForService(service, account: CredConstants.accessTokenKey)
        
        self.credentials = creds
        self.service = service
        self.username = username
        self.password = password
        self.accessToken = accessToken
    }
    
    
    //------------------------------------------------------------------------//
    // MARK: Utilities
    //------------------------------------------------------------------------//
    
    func isExpired() -> Bool {
        if credentials == nil || !self.credentials.isKindOfClass(NSDictionary) {
            return false
        } else {
            return (credentials.objectForKey(CredConstants.expiryDateKey) as NSDate).compare(NSDate()) == NSComparisonResult.OrderedAscending
        }
    }
    
    func isValid() -> Bool {
        return StringConstants.isStringValid(accessToken)
    }
    
    func postNotification() {
        if isValid() {
            println("[Debug] access token: \(accessToken)")
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationConstants.sessionOpened, object: nil)
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationConstants.sessionClosed, object: nil)
        }
    }
    
    func update(username: String, password: String, credentialObject: NSDictionary) {
        var accessToken: String = credentialObject.objectForKey(CredConstants.accessTokenKey) as String
        var expiresIn: String = credentialObject.objectForKey(CredConstants.expiresInKey) as String
        var scope: String = credentialObject.objectForKey(CredConstants.scopeKey) as String
        var tokenType: String = credentialObject.objectForKey(CredConstants.tokenTypeKey) as String
        var expiryDate: NSDate = NSDate(timeIntervalSinceNow: NSString(string: expiresIn).doubleValue)
        
        var creds: NSDictionary = NSDictionary(dictionary: [
            CredConstants.expiresInKey: expiresIn,
            CredConstants.expiryDateKey: expiryDate,
            CredConstants.scopeKey: scope,
            CredConstants.tokenTypeKey: tokenType
        ])
        
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setObject(creds, forKey: CredConstants.OAuthCredential)
        defaults.synchronize()
        
        SSKeychain.setPassword(username, forService: service, account: CredConstants.usernameKey)
        SSKeychain.setPassword(password, forService: service, account: CredConstants.passwordKey)
        SSKeychain.setPassword(accessToken, forService: service, account: CredConstants.accessTokenKey)
        
        self.credentials = creds
        self.username = username
        self.password = password
        self.accessToken = accessToken
    }
    
    func removeCredentials() {
        self.credentials = nil;
        self.username = nil;
        self.password = nil;
        self.accessToken = nil;
        
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(CredConstants.OAuthCredential)
        defaults.synchronize()
        
        SSKeychain.deletePasswordForService(service, account: CredConstants.usernameKey)
        SSKeychain.deletePasswordForService(service, account: CredConstants.passwordKey)
        SSKeychain.deletePasswordForService(service, account: CredConstants.accessTokenKey)
    }
}