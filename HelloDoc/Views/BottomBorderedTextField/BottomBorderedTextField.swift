//
//  BottomBorderedTextField.swift
//  HelloDoc
//
//  Created by Tan Mel Stychen on 2014/10/15.
//  Copyright (c) 2014年 HelloDoc. All rights reserved.
//

import UIKit


/******************************************************************************/
// MARK: -
// MARK: BottomBorderedTextField
// MARK: -
/******************************************************************************/


/// A UITextField with a bottom border
class BottomBorderedTextField: UITextField {
    
    var initialized: Bool = false
    var borderView: UIView!
    
    //------------------------------------------------------------------------//
    // MARK: Initialize
    //------------------------------------------------------------------------//
    
    override init() {
        super.init()
        initialize()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    func initialize() {
        if initialized == true { return }
        initialized = true
        
        borderView = UIView()
        borderView.backgroundColor = ColorConstants.borderGray
        addSubview(borderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var rect: CGRect = CGRectZero
        rect.origin.x = 0
        rect.origin.y = frame.size.height - LayoutConstants.borderWeight
        rect.size.height = LayoutConstants.borderWeight
        rect.size.width = frame.size.width
        
        borderView.frame = rect
    }
}